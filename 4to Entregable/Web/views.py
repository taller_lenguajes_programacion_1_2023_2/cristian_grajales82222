from django.shortcuts import render, redirect
from .models import Clientes

# Create your views here.
def pagInicio(request):
    return render(request, 'index.html')

def pagLogin(request):
    mensajeError = ""
    if request.method == 'POST':
        usuario = request.POST['usuario']
        contraseña = request.POST['contraseña']
        
        try:
            usuarioEncontrado = Clientes.objects.get(USUARIO = usuario)
            if usuarioEncontrado.CLAVE == contraseña:
                return redirect('/categorias/')
            else:
                mensajeError = "La contraseña ingresada es incorrecta."
                
        except:
            mensajeError = "El usuario ingresado no existe."
        
    return render(request, 'index2.html', {
        "error": mensajeError
    })

def pagRegistro(request):
    mensajeError = ""
    if request.method == 'POST':
        nombre = request.POST['nombre']
        primerApellido = request.POST['priApellido']
        segundoApellido = request.POST['segApellido']
        correo = request.POST['correo']
        usuario = request.POST['usuario']
        contraseña = request.POST['contraseña']
        
        try:
            usuarioExistente = Clientes.objects.get(USUARIO = usuario)
            mensajeError = "El usuario ingresado ya esta registrado"
        except:
            nuevoCliente = Clientes.objects.create(NOMBRE=nombre, P_APELLIDO=primerApellido, S_APELLIDO = segundoApellido, CORREO = correo, USUARIO = usuario, CLAVE = contraseña)
            return redirect('/login/')
    
    return render(request, 'index3.html', {
        "error": mensajeError
    })
