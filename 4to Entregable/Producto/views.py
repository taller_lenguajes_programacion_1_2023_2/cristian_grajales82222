from django.shortcuts import render

# Create your views here.

def pagCategorias(request):
    return render(request, 'categorias.html')

def infoCategorias(request, categoria):
    return render(request, 'infoCategorias.html', {
        "categoria":categoria
    })
    
def Carro(request,carrito):
    return render(request,'carrito.html',{
        "carrito":carrito
    })
    