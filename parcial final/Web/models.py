from django.db import models

# Create your models here.

class Cliente(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRES = models.CharField(max_length=50)
    P_APELLIDO = models.CharField(max_length=50)
    S_APELLIDO = models.CharField(max_length=50)
    CORREO = models.CharField(max_length=50)
    EDAD = models.CharField(max_length=50)
    USUARIO = models.CharField(max_length=50)
    CONTRASEÑA = models.CharField(max_length=50)
    
