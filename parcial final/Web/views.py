from django.shortcuts import render, redirect
from .models import Cliente

# Create your views here.

def pagLogin(request):
    mensajeError = ""
    if request.method == 'POST':
        usuario = request.POST['usuario']
        contraseña = request.POST['contraseña']
        
        try:
            clienteEncontrado = Cliente.objects.get(USUARIO=usuario)
            if clienteEncontrado.CONTRASEÑA == contraseña:
                return redirect('/tienda')
            else:
                mensajeError = "Contraseña Incorrecta."
        except:
            mensajeError = "Usuario no Registrado."
            
    return render(request, 'login.html', {
        "error":mensajeError
    })
        

def pagRegistro(request):
    mensajeError = ""
    if request.method == 'POST':
        nombres = request.POST['nombre']
        primerApellido = request.POST['priApellido']
        segundoApellido = request.POST['segApellido']
        correo = request.POST['correo']
        edad = request.POST['edad']
        usuario = request.POST['usuario']
        contraseña = request.POST['contraseña']
        
        try:
            usuarioenUso = Cliente.objects.get(USUARIO=usuario)
            mensajeError = "Usuario ya registrado"
        except:
            newCliente=Cliente.objects.create(NOMBRES = nombres, P_APELLIDO = primerApellido, S_APELLIDO = segundoApellido, CORREO = correo, EDAD = edad, USUARIO = usuario, CONTRASEÑA = contraseña)
            return redirect('/')

    return render(request, 'registro.html', {
        "error":mensajeError
    })
