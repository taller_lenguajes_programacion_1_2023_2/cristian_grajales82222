from django.shortcuts import render

# Create your views here.

def pagElementos(request):
    return render(request, 'tienda.html')

def pagDetalles(request):
    return render(request, 'detalles.html')
