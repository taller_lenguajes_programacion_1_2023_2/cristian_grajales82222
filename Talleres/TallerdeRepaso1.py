## TALLER DE REPASO 1
print("Taller de Repaso 1.\n")
print("1. Imprimir Hola, mundo!.")
print("Hola, mundo\n")

print("2. Variables y asignacion.")
nombre = "Cristian"
edad = 20
print(nombre, edad)
print("\n")

print("3. Tipos de datos basicos: int, float, string")
print(type(nombre))
print(type(edad))
print("\n")

print("4. Operaciones aritmeticas basicas.")
suma = 7+4
resta= 11-6
multiplicacion = 5*8
division = 20/4
print(" Suma:",suma, " Resta:",resta, " Multiplicacion:",multiplicacion," Division:",division)
print("\n")

print("5. Conversion entre tipo de datos.")
variableString = str(edad)
print(type(edad))
print("\n")

print("6. Solicitar entrada del usuario.")
nombreUsuario = input("Ingrese su nombre.")
numero = int(input("Ingrese un numero."))
print("Su nombre es: ", nombreUsuario,"\n")

print("7. condicional if")
print("El numero es:",numero)
if numero>7:
    print("1, 2, 3, 4, 5, 6, 7")
print("\n")
    
print("8. condicional if-else")
if numero%2:
    print("Su numero es impar.")
else:
    print("Su numero es par.")
print("\n")

print("9. Condicion if-elif-else.")  
if numero<10:
    print("Su numero es menor que 10 y el numero es: ", numero)
elif numero>10:
    print("Su numero es mayor que 10 y el numero es: ", numero)
else:
    print("Su numero es igual a: ", numero)
print("\n")
    
print("10. Bucle for")
lista=[7, 8, 9, 10, 11]
for i in lista:
    print(i)
    
print("Otro ejemplo")
for i in range(0, 10):
    print(i)
print("\n")
print("while")
print("11. Ciclo while")
numero=0
while numero<10:
    print (numero)
    numero=numero+1 
    
print("\n")

print("12. Uso de break y continue")
for i in range(10, 15):
    print(i)
    continue
print("Uso break")
numero2=0
while numero2<10:
    print (numero)
    numero2=numero2+1 
    if numero2==5:
        break 
    print("\n")

print("13. Listas y sus operaciones basicas")
lista1 = [1, 2, 3, 4, 5]
lista1.extend(lista1)
lista1.append(6)
lista1.insert(3, 3.5)
lista1.remove(4)
print(lista1)
print("\n")

print("14. tuplas y su inmutabilidad")
tupla = (7, 7, 8, 9, 10, 11, "Cris")
tupla.count(7)
tupla.index("Cris")
print(tupla)
print("\n")

print("15. Conjuntos y operaciones")
conjunto1 = {1, 2, 3, 4, 5, 6}
conjunto2 = {5, 6, 7, 8, 9, 10}
print("Union: ", conjunto1.union(conjunto2))
print("Intercepcion: ", conjunto1.intersection(conjunto2))
print("Diferencia: ", conjunto1.difference(conjunto2))
print("\n")

print("16. Diccionario")
diccionario={
    "Nombre": "Cristian",
    "Apellido": "Grajales",
    "Edad": 20
    }
print(diccionario.keys())
print(diccionario.values())
print(diccionario.get("Nombre"))
print("\n")

print("17.List funciones basicas")
lista2 = [1, 2, 3, 4, 5]
lista2.extend(lista1)
lista2.append(6)
lista2.insert(3, 3.5)
lista2.remove(4)
print(lista2)
print("\n")

print("18. Leer un archivo de texto")
with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/punto18.txt", "w") as txt:
    txt.write("Cristian Camilo Grajales Serna")
with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/punto18.txt", "a") as txt:
    txt.write("\nTengo 20 años")
with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/punto18.txt", "r") as txt:
    print(txt.read())
print("\n")


print("19. Escribir en archivo de texto")
print("\n")

print("20. Modos de apertura r, w, a, rb, wb")
#w es para empezar
#a es para agregar
#r Es para leer
print("\n")

print("21. Trabajar con archivos JSON")
import pandas as pd
archivojson =pd.DataFrame( {
    "Nombres":["Cristian", "Andres", "Juanjo"],
    "Edades":[20, 20, 19]
})
archivojson.to_json("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto21.json")

print("\n")

print("22. Leer un archivo CSV")
archivocsv=pd.read_csv("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto22.txt",sep=";")
print(archivocsv)
print("\n")

print("23. Filtrar datos en un DataFrame")
filtro=archivocsv["Fecha"]
print(filtro)
print("\n")

print("24. Operaciones basicas: sum(), mean(), max(), min()")
import numpy as np
punto24=[10, 20, 30, 40, 50]
print(np.sum(punto24))
print(np.mean(punto24))
print(np.max(punto24))
print(np.min(punto24))
print("\n")

print("25. Uso de iloc y loc")
punto25=pd.DataFrame({
    "Nombres":["Cristian", "Andres", "Juanjo"],
    "Edades":[20, 20, 19]
})
print(punto25.iloc[0])
print(punto25.loc[1]["Nombres"])
print(punto25.loc[1]["Edades"])
print("\n")

print("26. Agrupar datos con groupby")
punto25.groupby("Edades")
for Edades in punto25.groupby("Edades"):
    print(Edades)
print("\n")

print("27. Unir Dataframes con merge y concat")
punto27=pd.DataFrame({
    "Nombres":["Cristian", "Andres", "Juanjo"],
    "Documentos":[1001, 1002, 1003]
})
print("concat")
print(pd.concat([punto25, punto27], axis=1))
print("merge")
print(pd.merge(punto25, punto27, on="Nombres"))
print("\n")

print("28. Manipular series temporales")
valores=(1, 2, 3 , 4, 5)
print(pd.Series(valores))
print("\n")

print("29. Exportar un Dataframe a CSV")
punto27.to_csv("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto29.csv")
print("\n")

print("30. Convertir un DataFrame a JSON")
punto27.to_json("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto30.json")
print("\n")

print("31. Lee una tabla HTML y guardalo en un DataFrame.pd.read_html()")
netflix=pd.read_html("https://help.netflix.com/es/node/24926")[0]
print(netflix)
print("\n")

print("32. Guardar la tabla html en un excel")
netflix.to_excel("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto32.xlsx")
print("\n")

print("33. Exporta los primeros 10 registros del DataFrame anterior a un archivo CSV")
punto33=pd.read_html("https://www.colombia.com/cambio-moneda/monedas-del-mundo/")[0]
punto33.head(10).to_csv("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto33.csv")
print("\n")

print("34. Lee tres tablas HTML y guardalas en tres hojas diferentes de un archivo Excel")
punto34=pd.read_html("https://www.worldometers.info/gdp/gdp-per-capita/")[0]
with pd.ExcelWriter("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto34.xlsx") as writer:
    netflix.to_excel(writer, sheet_name="netflix")
    punto33.to_excel(writer, sheet_name="Monedas")
    punto34.to_excel(writer, sheet_name="GPD")
print("\n")

print("35. Lee un archivo excel, filtra los registros donde la columna edad sea mayor a 30 y guardalos")
filtroEdades=punto25.loc[punto25["Edades"]>19]
print(filtroEdades)
filtroEdades.to_csv("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto35.csv")
print("\n")

print("36. Lee un archivo CSV, selecciona solo las columnas nombre y ciudad y guardalas en un aarchivo e")
filtroNetflix=netflix["Planes de Netflix"]
filtroNetflix.to_excel("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto36.xlsx")
print("\n")

print("37. Lee la primer y tercera hoja del archivo excel y combinalas en un DataFrame")
excel=pd.ExcelFile("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto34.xlsx")
Monedas=pd.read_excel(excel, "Monedas")
gpd=pd.read_excel(excel, "GPD")
combinar=pd.concat([Monedas, gpd])
combinar.to_excel("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Taller de Lenguajes 1/cristian_grajales82222/Talleres/Punto37.xlsx")