import tkinter as tk
from tkinter import messagebox, PhotoImage
import sv_ttk

class Interfaz():
    def __init__ (self):
        self.ventana=tk.Tk()
        self.ventana.title("Prueba")
        self.ventana.iconbitmap("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Interfaz/icono.ico")
        self.ventana.resizable(False, False)
        sv_ttk.use_dark_theme()
        self.campoTexto()
        self.menu()
        self.ventana.mainloop()
        
    def campoTexto(self):
        #LABELS
        self.label_Ingrese = tk.Label(self.ventana, text="Ingrese su edad.", font=("Times New Roman", 20, "bold"))
        self.label_Ingrese.grid(row=0, column=0, padx=20, pady=0)
        
        #Inserta imagenes
        self.imagen = PhotoImage(file=("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Interfaz/imagen1.png"))
        self.Label_imagen = tk.Label(image = self.imagen)
        self.Label_imagen.grid(row=3, column=0)
        
        #INPUTS
        self.varEdad = tk.IntVar()
        self.input_edad = tk.Entry(self.ventana , textvariable=self.varEdad, background="white", width=50)
        self.input_edad.grid(row=1, column=0, padx=20, pady=20)
        
        #BOTONES
        self.boton_aceptar = tk.Button(self.ventana, text="Aceptar", command=self.validarEdad, background="green")
        self.boton_aceptar.grid(row=2, column=0, padx=20, pady=20)
        
    def menu(self):
        self.varMenu = tk.Menu(tearoff=0)
        self.menuAceptar = tk.Menu(tearoff=0)
        
        self.varMenu.add_cascade(label="Archivo", menu=self.menuAceptar)
        self.menuAceptar.add_command(label="Aceptar", command=self.validarEdad)
        self.ventana.config(menu=self.varMenu)
        
        
    def validarEdad(self):
        if self.varEdad.get()>=18:
            messagebox.showinfo("Resultado", "Usted es mayor de edad.")
        else:
            messagebox.showinfo("Resultado", "Usted es menor de edad.")

def main ():
    
    aplicacion = Interfaz()
    
main()