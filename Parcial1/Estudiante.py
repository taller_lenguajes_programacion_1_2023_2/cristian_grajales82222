from conexion import conexion

class Estudiante(conexion):
    
    def __init__(self, nombre="", apellido="", dni="", fechaIngreso="", carrera="", correoelectronico=""):
        self.nombre = nombre
        self.apellido = apellido
        self.dni = dni
        self.fechaIngreso = fechaIngreso
        self.carrera = carrera
        self.correoelectronico = correoelectronico
        
    def setnombre(self, nombre1):
        """Me establece el dato

            Los guarda en:
                nombre1
        """
        self.nombre = nombre1
        
    def getnombre(self):
        """Recibe los datos
            
        Retorna:
            nombre
        """
        return self.nombre
    
    def setapellido(self, apellido1):
        """Me establece el dato

            Los guarda en:
                apellido1
        """
        self.apellido = apellido1
        
    def getapellido(self):
        """Recibe los datos
            
        Retorna:
            apellido
        """
        return self.apellido
    
    def setdni(self, dni1):
        """Me establece el dato

            Los guarda en:
                dni1
        """
        self.dni = dni1
        
    def getdni(self):
        """Recibe los datos
            
        Retorna:
            dni
        """
        return self.dni
    
    def setfechaIngreso(self, fechaIngreso1):
        """Me establece el dato

            Los guarda en:
                fechaIngreso1
        """
        self.fechaIngreso = fechaIngreso1
        
    def getfechaIngreso(self):
        """Recibe los datos
            
        Retorna:
            fechaIngreso
        """
        return self.fechaIngreso
    
    def setcarrera(self, carrera1):
        """Me establece el dato

            Los guarda en:
                carrera1
        """
        self.carrera = carrera1
        
    def getcarrera(self):
        """Recibe los datos
            
        Retorna:
            carrera
        """
        return self.carrera
    
    def setcorreoelectronico(self, correoelectronico1):
        """Me establece el dato

            Los guarda en:
                correoelectronico1
        """
        self.correoelectronico = correoelectronico1
        
    def getcorreoelectronico(self):
        """Recibe los datos
            
        Retorna:
            correoElectronico
        """
        return self.correoelectronico
    

class TablaEstudiante(conexion):
    """Instanciar otra clase
        
        Argumentos:
            Llama el crear de la clase conexion y me crear la base de datos
    """
    def CrearTablaEstudiante(self):
        """Crea tabla
            Argumentos:
                Me genera la base de datos de la clase estudiante.
        """ 
        self.Crear("Estudiante", "ColumnaEstudiante")
            
    def AgregarDatos(self, nombre, apellido, dni, fechaIngreso, carrera, correoelectronico):
        """Agregar Datos
        
            Argumentos:
                Me añade los datos deseados a la base de datos estudiante.
        """
        InformacionEstudiante=[
            (f"{nombre}", f"{apellido}", f"{dni}", f"{fechaIngreso}", f"{carrera}", f"{correoelectronico}")
        ]
        self.AgregarInformacion("Estudiante", "?, ?, ?, ?, ?, ?", InformacionEstudiante)
        
    def EliminarDatos(self, ID):
        """Eliminar Datos

        Argumentos:
            Me elimina los datos deseados
        """
        if self.EliminarInformacion("Estudiante", ID):
            print("Eliminado correctamente.")
        else:
            print("No se ha podido eliminar.")