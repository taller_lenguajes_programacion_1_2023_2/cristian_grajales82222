from Universidad import Universidad
from Estudiante import Estudiante
from conexion import conexion

class Matricula(conexion):
    
    def __init__(self, fechaMatricula="", estudiante="", universidad="", notaFinal=""):
        self.fechaMatricula = fechaMatricula
        self.estudiante = estudiante
        self.universidad = universidad
        self.notaFinal = notaFinal
        
    def setfechaMatricula(self, fechaMatricula1):
        """Me establece el dato

            Los guarda en:
                fechaMatricula1
        """
        self.fechaMatricula = fechaMatricula1
        
    def getfechaMatricula(self):
        """Recibe los datos
            
        Retorna:
            fechaMatricula
        """
        return self.fechaMatricula
    
    def setestudiante(self, estudiante1):
        """Me establece el dato

            Los guarda en:
                apellidoestudiante1
        """
        self.estudiante = estudiante1
        
    def getestudiante(self):
        """Recibe los datos
            
        Retorna:
            estudiante
        """
        return self.estudiante
    
    def setuniversidad(self, universidad):
        """Me establece el dato

            Los guarda en:
                universidad
        """
        self.universidad = universidad
        
    def getuniversidad(self):
        """Recibe los datos
            
        Retorna:
            universidad
        """
        return self.universidad
    
    def setnotaFinal(self, notaFinal1):
        """Me establece el dato

            Los guarda en:
                notaFinal1
        """
        self.notaFinal = notaFinal1
        
    def getnotaFinal(self):
        """Recibe los datos
            
        Retorna:
            notaFinal
        """
        return self.notaFinal

class TablaMatricula(conexion):
    """Instanciar otra clase
        
        Argumentos:
            Llama el crear de la clase conexion y me crear la base de datos
    """
    def CrearTablaMatricula(self):
        """Crea tabla
            Argumentos:
                Me genera la base de datos de la clase matricula.
        """ 
        self.Crear("Matricula", "ColumnaMatricula")
            
    def AgregarDatos(self, fechaMatricula, estudiante, universidad, notaFinal):
        """Agregar Datos
        
            Argumentos:
                Me añade los datos deseados a la base de datos matricula.
        """
        InformacionMatricula=[
            (f"{fechaMatricula}", f"{estudiante}", f"{universidad}", f"{notaFinal}")
        ]
        self.AgregarInformacion("Matricula", "?, ?, ?, ?", InformacionMatricula)
        
    def EliminarDatos(self, ID):
        """Eliminar Datos

        Argumentos:
            Me elimina los datos deseados
        """
        if self.EliminarInformacion("Matricula", ID):
            print("Eliminado correctamente.")
        else:
            print("No se ha podido eliminar.")