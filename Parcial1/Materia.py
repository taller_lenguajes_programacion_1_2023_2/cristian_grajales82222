from Universidad import Universidad
from Estudiante import Estudiante
from Matricula import Matricula
from conexion import conexion

class Materia(conexion):
        
    def setnombre(self, nombre1):
        """Me establece el dato

            Los guarda en:
                nombre1
        """
        self.nombre = nombre1
        
    def getnombre(self):
        """Recibe los datos
            
        Retorna:
            nombre
        """
        return self.nombre
    
    def setcodigo(self, codigo1):
        """Me establece el dato

            Los guarda en:
                codigo1
        """
        self.codigo = codigo1
        
    def getcodigo(self):
        """Recibe los datos
            
        Retorna:
            codigo
        """
        return self.codigo
    
    def setsemestre(self, semestre1):
        """Me establece el dato

            Los guarda en:
                semestre1
        """
        self.semestre = semestre1
        
    def getsemestre(self):
        """Recibe los datos
            
        Retorna:
            semestre
        """
        return self.semestre
    
    def sethorasSemanales(self, horasSemanales1):
        """Me establece el dato

            Los guarda en:
                horasSemanales1
        """
        self.horasSemanales = horasSemanales1
        
    def gethorasSemanales(self):
        """Recibe los datos
            
        Retorna:
            horasSemanales
        """
        return self.horasSemanales
    
    def setprograma(self, programa1):
        """Me establece el dato

            Los guarda en:
                programa1
        """
        self.programa = programa1
        
    def getprograma(self):
        """Recibe los datos
            
        Retorna:
            programa
        """
        return self.programa
    
    def setcreditos(self, creditos1):
        """Me establece el dato

            Los guarda en:
                creditos1
        """
        self.creditos = creditos1
        
    def getcreditos(self):
        """Recibe los datos
            
        Retorna:
            creditos
        """
        return self.creditos
    

class TablaMateria(conexion):
    """Instanciar otra clase
        
        Argumentos:
            Llama el crear de la clase conexion y me crear la base de datos
    """
    def CrearTablaMateria(self):
        """Crea tabla
            Argumentos:
                Me genera la base de datos de la clase materia.
        """ 
        self.Crear("Materia", "ColumnaMateria")
            
    def AgregarDatos(self, nombre, codigo, semestre, horasSemanales, programa, creditos):
        """Agregar Datos
        
            Argumentos:
                Me añade los datos deseados a la base de datos materia..
        """
        InformacionMateria=[
            (f"{nombre}", f"{codigo}", f"{semestre}", f"{horasSemanales}", f"{programa}", f"{creditos}")
        ]
        self.AgregarInformacion("Materia", "?, ?, ?, ?, ?, ?", InformacionMateria)
        
    def EliminarDatos(self, ID):
        """Eliminar Datos

        Argumentos:
            Me elimina los datos deseados
        """
        if self.EliminarInformacion("Materia", ID):
            print("Eliminado correctamente.")
        else:
            print("No se ha podido eliminar.")