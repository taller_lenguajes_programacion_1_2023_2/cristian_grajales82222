import sqlite3
import json

class conexion():
    
    def __init__(self):
        self.baseDatos=sqlite3.connect("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Parcial1/BaseDatos.sqlite")
        self.Apuntador=self.baseDatos.cursor()

        with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Parcial1/Query.json", "r") as Datos:
            self.Queries=json.load(Datos)
            
    def Crear(self, NombreTabla="", Columnas=""):
        """Crear bases de datos

        Argumentos:
            Me permite crear las base de datos deseadas.
        """
        if NombreTabla!="" and Columnas!="":
            Info=self.Queries["Crear"].format(NombreTabla, self.Queries[Columnas])
            self.Apuntador.execute(Info)
            self.baseDatos.commit()
            print("La base de datos", NombreTabla, " se ha creado correctamente.")
            
    def AgregarInformacion(self, Tabla="", Interrogantes="", Valores=""):
        """Agregar informacion

        Argumentos:
            Me permite agregar la informacion suministrada a las diferentes bases de datos.
        """
        if Tabla!="" and Valores!="":
            Info=self.Queries["AgregarInformacion"].format(Tabla, Interrogantes)
            self.Apuntador.executemany(Info, Valores)
            self.baseDatos.commit()
            print("Los datos se agregaron correctamente la base de datos: ", Tabla)
            
    def BuscarPorId (self,ID, Tabla ):
        Buscador=self.Queries["Ver"].format(Tabla,"Id",ID)
        self.Apuntador.execute(Buscador)
        Buscador = self.Apuntador.fetchone()[0]   
        return Buscador
        
    def EliminarInformacion (self, Tabla="", Id=0):
        if Tabla!="" and Id!=0:
            if(self.BuscarPorId(Id, Tabla)):
                dato=self.Queries["Eliminar"].format(Tabla,"ID",Id)
                self.Apuntador.execute(dato)                    
                self.baseDatos.commit()
                print("Los datos se han eliminado correctamente")
                return True
            else:
                print("NO ENCONTRADO")  
                return False
        else:
            print("ERROR")  
            return False   
        