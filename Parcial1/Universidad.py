
from conexion import conexion

class Universidad(conexion):
    
    def __init__(self, Nombre="", direccion="", rector="", fundacion="", telefono="", correoElectronico=""):
        self.Nombre = Nombre
        self.direccion = direccion
        self.rector = rector
        self.fundacion = fundacion
        self.telefono = telefono
        self.correoElectronico = correoElectronico
        
        
    def setNombre(self, Nombre1):
        """Me establece el dato

            Los guarda en:
                Nombre1
        """
        self.Nombre = Nombre1
        
    def getNombre(self):
        """Recibe los datos
            
        Retorna:
            Nombre
        """
        return self.Nombre
    
    def setdireccion(self, direccion1):
        """Me establece el dato

            Los guarda en:
                direccion1
        """
        self.direccion = direccion1
        
    def getdireccion(self):
        """Recibe los datos
            
        Retorna:
            direccion
        """
        return self.direccion
    
    def setrector(self, rector1):
        """Me establece el dato

            Los guarda en:
                rector1
        """
        self.rector = rector1
        
    def getrector(self):
        """Recibe los datos
            
        Retorna:
            rector
        """
        return self.rector
    
    def setfundacion(self, fundacion1):
        """Me establece el dato

            Los guarda en:
                fundacion1
        """
        self.fundacion = fundacion1
        
    def getfundacion(self):
        """Recibe los datos
            
        Retorna:
            fundacion
        """
        return self.fundacion
    
    def settelefono(self, telefono1):
        """Me establece el dato

            Los guarda en:
                telefono1
        """
        self.telefono = telefono1
        
    def gettelefono(self):
        """Recibe los datos
            
        Retorna:
            telefono
        """
        return self.telefono
    
    def setcorreoElectronico(self, correoElectronico1):
        """Me establece el dato

            Los guarda en:
                correoElectronico1
        """
        self.correoElectronico = correoElectronico1
        
    def getcorreoElectronico(self):
        """Recibe los datos
            
        Retorna:
            correoElectronico
        """
        return self.correoElectronico
    

class TablaUniversidad(conexion):
    """Instanciar otra clase
        
        Argumentos:
            Llama el crear de la clase conexion y me crear la base de datos
    """
    def CrearTablaUniversidad(self):
        """Crea tabla
            Argumentos:
                Me genera la base de datos de la clase universidad.
        """ 
        self.Crear("Universidad", "ColumnaUniversidad")
            
    def AgregarDatos(self, Nombre, direccion, rector, fundacion, telefono, correoElectronico):
        """Agregar Datos
        
            Argumentos:
                Me añade los datos deseados a la base de datos universidad.
        """
        InformacionUniversidad=[
            (f"{Nombre}", f"{direccion}", f"{rector}", f"{fundacion}", f"{telefono}", f"{correoElectronico}")
        ]
        self.AgregarInformacion("Universidad", "?, ?, ?, ?, ?, ?", InformacionUniversidad)
        
    def EliminarDatos(self, ID):
        """Eliminar Datos

        Argumentos:
            Me elimina los datos deseados
        """
        if self.EliminarInformacion("Universidad", ID):
            print("Eliminado correctamente.")
        else:
            print("No se ha podido eliminar.")