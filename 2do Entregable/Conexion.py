import sqlite3
import json

class conexion():
    
    def __init__(self):
        self.baseDatos=sqlite3.connect("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/BaseDatos.sqlite")
        self.Apuntador=self.baseDatos.cursor()

        with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/Query.json", "r") as Datos:
            self.Queries=json.load(Datos)
            
    def Crear(self, NombreTabla="", Columnas=""):
        """Crear bases de datos

        Argumentos:
            Me permite crear las base de datos deseadas.
        """
        if NombreTabla!="" and Columnas!="":
            Info=self.Queries["Crear"].format(NombreTabla, self.Queries[Columnas])
            self.Apuntador.execute(Info)
            self.baseDatos.commit()
            print("La base de datos", NombreTabla, " se ha creado correctamente.")
            
    def AgregarInformacion(self, Tabla="", Interrogantes="", Valores=""):
        """Agregar informacion

        Argumentos:
            Me permite agregar la informacion suministrada a las diferentes bases de datos.
        """
        if Tabla!="" and Valores!="":
            Info=self.Queries["AgregarInformacion"].format(Tabla, Interrogantes)
            self.Apuntador.executemany(Info, Valores)
            self.baseDatos.commit()
            print("Los datos se agregaron correctamente la base de datos: ", Tabla)
       
    def LeerInformacion(self, Tabla="", Interrogantes="", Valores=""):
        """Lee Datos

        Argumentos:
            Me lee los datos de las bases de datos
        """
        if Tabla!="" and Valores!="":
            Info=self.Queries["LeerInformacion"].format(Tabla, Interrogantes)
            self.Apuntador.executemany(Info, Valores)
            self.baseDatos.commit()
 
    def ActualizarInformacion(self, Tabla="", Interrogantes="", Valores=""):
        """Actualiza Informacion

        Argumentos:
            Me permite actualizar la informacion deseada de las bases de datos
        """
        if Tabla!="" and Valores!="":
            Info=self.Queries["ActualizarInformacion"].format(Tabla, Interrogantes)
            self.Apuntador.executemany(Info, Valores)
            self.baseDatos.commit()
            print("La base de datos", Tabla, "se ha actualizado correctamente")
            
    def EliminarInformacion(self, Tabla="", Interrogantes="", Valores=""):
        """Elimina Informacion

        Argumentos:
            Me permite eliminar la informacion deseada de las bases de datos
        """
        if Tabla!="" and Valores!="":
            Info=self.Queries["EliminarInformacion"].format(Tabla, Interrogantes)
            self.Apuntador.executemany(Info, Valores)
            self.baseDatos.commit()
            print("Los datos se han eliminado correctamente")