from Futbol import TablaFutbol
from EquiposFutbol import TablaEquiposEquiposFutbol


CrearF = TablaFutbol()
CrearF.CrearTablaFutbol()
CrearF.AgregarDatos("Liga Betplay", 1948, "CONMEBOL", "Dimayor", "Colombia", "Medellin")
CrearF.AgregarDatos("Premier League", 1888, "UEFA", "The FA", "Inglaterra", "Liverpool")
CrearF.AgregarDatos("La Liga Santander", 1928, "UEFA", "RFEF", "España", "Madrid")
CrearEF = TablaEquiposEquiposFutbol()
CrearEF.CrearTablaEquiposFutbol()
CrearEF.AgregarDatos("Atletico Nacional", "Primera A", "William Amaral", "Victor Hugo Aristizabal", 32, "Atanasio Girardot")
CrearEF.AgregarDatos("Liverpool FC", "Maxima Categoria de Inglaterra", "Jurgen Klopp", "Ian Rush", 53, "Anfield")
CrearEF.AgregarDatos("Real Madrid CF", "Primera Division de España", "Carlo Ancelotti", "Cristiano Ronaldo", 97, "Santiago Bernabeu")