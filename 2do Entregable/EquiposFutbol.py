import pandas as pd
import sqlite3
from Futbol import futbol
from Conexion import conexion

class equiposFutbol(futbol):
            
    def __init__(self,  NombreLiga="", AñoCreacionLiga="", Confederacion="",OrganismoqueRige="", PaisEquipo="", CiudadEquipo="", Equipo="", CategoriaLigaEquipo="", DirectorTecnico="", GoleadorHistorico="", Titulos="", Estadio=""):
        """METODOS
        
        Argumentos:
            NombreLiga (String): Nombre de la liga donde juega el equipo.
            AñoCreacionLiga (Integer): Año en el que se creo la liga donde juega el club.
            Confederacion(String): Confederacion a la que pertenece el equipo y liga
            OrganismoqueRige (string): Organismo que controla la liga donde juega el equipo.
            PaisEquipo (String): Pais donde juega el equipo.
            CiudadEquipo (String): Ciudad a la que pertenece el equipo .
            Equipo (String): Nombre del equipo que desea ingresar.
            CategoriaLigaEquipo (String): Categoria en la juega el equipo.
            DirectorTecnico (String): Tecnico que dirige el equipo.
            GoleadorHistorico (String): Jugador con el mayor numero de goles en la historia del club.
            Titulos (Integer): Numero de titulos totales conseguidos por el club.
            Estadio (String): Nombre del estadio en el que oficia de local el equipo.
        """
        Futbolexc=pd.read_excel("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/Excel2do_entregable.xlsx", "ClasePaisLiga", index_col="ID")
        equiposFutbolexc=pd.read_excel("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/excel2doE.xlsx", "ClaseEquipos", index_col="ID")
        
        super().__init__(Futbolexc.loc[id, NombreLiga], Futbolexc.loc[AñoCreacionLiga], Futbolexc.loc[Confederacion], Futbolexc.loc[OrganismoqueRige], Futbolexc.loc[PaisEquipo], Futbolexc.loc[CiudadEquipo])
        
        self.Equipo = equiposFutbolexc.loc[id, Equipo]
        self.CategoriaLigaEquipo = equiposFutbolexc.loc[id, CategoriaLigaEquipo]
        self.DirectorTecnico = equiposFutbolexc.loc[id, DirectorTecnico]
        self.GoleadorHistorico = equiposFutbolexc.loc[id, GoleadorHistorico]
        self.Titulos = int(equiposFutbolexc.loc[id, Titulos])
        self.Estadio = equiposFutbolexc.loc[id, Estadio]
    def __init__(self):
        """Migracion de los datos de excel a SQlite
        """
        archivoExcel="C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/Excel2do_entregable.xlsx"
        nombreTabla="BaseDatos.sqlite"
        conectar=sqlite3.connect("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/BaseDatos.sqlite")
        datos=pd.read_excel(archivoExcel)
        datos.to_sql(nombreTabla, conectar, if_exists="replace", index=False)
        
    def setAñoCreacionLiga(self, AñoCreacionLiga1):
        """Me establece el dato

            Los guarda en:
                AñoCreacionLiga1
        """
        self.AñoCreacionLiga = AñoCreacionLiga1
        
        
    def getAñoCreacionLiga(self):
        """Recibe los datos
            
        Retorna:
            AñoCreacionLiga
        """
        return self.AñoCreacionLiga

    
    def setNombreLiga(self, NombreLiga1):
        """Me establece el dato

            Los guarda en:
                NombreLiga1
        """
        self.NombreLiga = NombreLiga1
        
    def getNombreLiga(self):
        """Recibe los datos
            
        Retorna:
            NombreLiga
        """
        return self.NombreLiga
    
    def setConfederacion(self, Confederacion1):
        """Me establece el dato

            Los guarda en:
                Confederacion1
        """
        self.Confederaciono = Confederacion1
        
    def getConfederacion(self):
        """Recibe los datos
            
        Retorna:
            Confederacion
        """
        return self.Confederacion
    
    def setOrganismoqueRige(self, OrganismoqueRige1):
        """Me establece el dato

            Los guarda en:
                OrganismoqueRige1
        """
        self.OrganismoqueRige = OrganismoqueRige1
        
    def getOrganismoqueRige(self):
        """Recibe los datos
            
        Retorna:
            OrganismoqueRige
        """
        return self.OrganismoqueRige
    
    def setPais(self, PaisEquipo1):
        """Me establece el dato

            Los guarda en:
                PaisEquipo1
        """
        self.Pais = PaisEquipo1
        
    def getPaisEquipo(self):
        """Recibe los datos
            
        Retorna:
            Pais
        """
        return self.PaisEquipo
    
    def setCiudad(self, Ciudad1):
        """Me establece el dato

            Los guarda en:
                Ciudad1
        """
        self.Ciudad = Ciudad1
        
    def getCiudad(self):
        """Recibe los datos
            
        Retorna:
            Ciudad
        """
        return self.Ciudad
    
    def setEquipo(self, Equipo1):
        """Me establece el dato

            Los guarda en:
                Equipo1
        """
        self.Equipo = Equipo1
        
    def getEquipo(self):
        """Recibe los datos
            
        Retorna:
            Equipo
        """
        return self.Equipo
    
    def setCategoriaLigaEquipo(self, CategoriaLigaEquipo1):
        """Me establece el dato

            Los guarda en:
                CategoriaLigaEquipo1
        """
        self.CategoriaLigaEquipo = CategoriaLigaEquipo1
        
    def getCategoriaLigaEquipo(self):
        """Recibe los datos
            
        Retorna:
            CategoriaLigaEquipo
        """
        return self.CategoriaLigaEquipo
    
    def setDirectorTecnico(self, DirectorTecnico1):
        """Me establece el dato

            Los guarda en:
                DirectorTecnico1
        """
        self.DirectorTecnico = DirectorTecnico1
        
    def getDirectorTecnico(self):
        """Recibe los datos
            
        Retorna:
            DirectorTecnico
        """
        return self.DirectorTecnico
    
    def setGoleadorHistorico(self, GoleadorHistorico1):
        """Me establece el dato

            Los guarda en:
                    GoleadorHistorico1

        """
        self.GoleadorHistorico = GoleadorHistorico1
        
    def getGoleadorHistorico(self):
        """Recibe los datos
            
        Retorna:
            GoleadorHistorico
        """
        return self.GoleadorHistorico
    
    def setTitulos(self, Titulos1):
        """Me establece el dato

            Los guarda en:
                    Titulos1

        """
        self.Titulos = Titulos1
        
    def getTitulos(self):
        """Recibe los datos
            
        Retorna:
            Titulos
        """
        return self.Titulos
    
    def setEstadio(self, Estadio1):
        """Me establece el dato

            Los guarda en:
                    Estadio1

        """
        self.Estadio = Estadio1
        
    def getEstadio(self):
        """Recibe los datos
            
        Retorna:
            Estadio
        """
        return self.Estadio
    
class TablaEquiposEquiposFutbol(conexion):
    """Instanciar otra clase
        
        Argumentos:
            Llama el crear de la clase conexion y me crear la base de datos
        """
    def CrearTablaEquiposFutbol(self):
        """Crea tabla
        
            Argumentos:
                Me genera la base de datos de la clase EquiposFutbol.
        """ 
        self.Crear("EquiposFutbol", "ColumnaEquiposFutbol")
            
    def AgregarDatos(self, Equipo, CategoriaLigaEquipo, DirectorTecnico, GoleadorHistorico, Titulos, Estadio):
        """Agregar Datos
        
            Argumentos:
                Me añade los datos deseados a la base de datos EquiposFutbol
        """
        InformacionEquipos=[
            (f"{Equipo}", f"{CategoriaLigaEquipo}", f"{DirectorTecnico}", f"{GoleadorHistorico}", f"{Titulos}", f"{Estadio}")
        ]
        self.AgregarInformacion("EquiposFutbol", "?, ?, ?, ?, ?, ?", InformacionEquipos)
        