import sqlite3
from Conexion import conexion
import pandas as pd

class futbol:
    def __init__(self):
        """Migracion de los datos de excel a SQlite
        """
        archivoExcel="C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/Excel2do_entregable.xlsx"
        nombreTabla="BaseDatos.sqlite"
        conectar=sqlite3.connect("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Entregable 2/BaseDatos.sqlite")
        datos=pd.read_excel(archivoExcel)
        datos.to_sql(nombreTabla, conectar, if_exists="replace", index=False)

    def setAñoCreacionLiga(self, AñoCreacionLiga1):
        """Me establece el dato

            Los guarda en:
                AñoCreacionLiga1
        """
        self.AñoCreacionLiga = AñoCreacionLiga1
        
    def getAñoCreacionLiga(self):
        """Recibe los datos
            
        Retorna:
            AñoCreacionLiga
        """
        return self.AñoCreacionLiga
    
    def setNombreLiga(self, NombreLiga1):
        """Me establece el dato

            Los guarda en:
                NombreLiga1
        """
        self.NombreLiga = NombreLiga1
        
    def getNombreLiga(self):
        """Recibe los datos
            
        Retorna:
            NombreLiga
        """
        return self.NombreLiga
    
    def setConfederacion(self, Confederacion1):
        """Me establece el dato

            Los guarda en:
                Confederacion1
        """
        self.Confederacion = Confederacion1
        
    def getConfederacion(self):
        """Recibe los datos
            
        Retorna:
            Confederacion
        """
        return self.Confederacion
    
    def setOrganismoqueRige(self, OrganismoqueRige1):
        """Me establece el dato

            Los guarda en:
                OrganismoqueRige1
        """
        self.OrganismoqueRige = OrganismoqueRige1
        
    def getOrganismoqueRige(self):
        """Recibe los datos
            
        Retorna:
            OrganismoqueRige
        """
        return self.OrganismoqueRige
    
    def setPaisEquipo(self, PaisEquipo1):
        """Me establece el dato

            Los guarda en:
                Pais1
        """
        self.PaisEquipo = PaisEquipo1
        
    def getPaisEquipo(self):
        """Recibe los datos
            
        Retorna:
            Pais
        """
        return self.PaisEquipo
    
    def setCiudadEquipo(self, CiudadEquipo1):
        """Me establece el dato

            Los guarda en:
                Ciudad1
        """
        self.CiudadEquipo = CiudadEquipo1
        
    def getCiudadEquipo(self):
        """Recibe los datos
            
        Retorna:
            Ciudad
        """
        return self.CiudadEquipo
    

class TablaFutbol(conexion):
    """Instanciar otra clase
        
        Argumentos:
            Llama el crear de la clase conexion y me crear la base de datos
    """
    def CrearTablaFutbol(self):
        """Crea tabla
            Argumentos:
                Me genera la base de datos de la clase Futbol.
        """ 
        self.Crear("Futbol", "ColumnaFutbol")
            
    def AgregarDatos(self, NombreLiga, AñoCreacionLiga, Confederacion, OrganismoqueRige, PaisEquipo, CiudadEquipo):
        """Agregar Datos
        
            Argumentos:
                Me añade los datos deseados a la base de datos Futbol
        """
        InformacionFutbol=[
            (f"{NombreLiga}", f"{AñoCreacionLiga}", f"{Confederacion}", f"{OrganismoqueRige}", f"{PaisEquipo}", f"{CiudadEquipo}")
        ]
        self.AgregarInformacion("Futbol", "?, ?, ?, ?, ?, ?", InformacionFutbol)
        
    def LeerDatos(self, TablaFutbol):
        """Lectura de datos

        Argumentos:
            Me permite leer la informacionde la base de datos
        """
        self.LeerInformacion(TablaFutbol)
        valor=self.Apuntador.fetchall()
        print(valor)
        
    def ActualizarDatos(self, TablaFutbol, ColumnaFutbol, LigaBetplay, ID):
        """Actualizacion de Datos

        Argumentos:
            Me actualiza los datos deseados   
        """
        self.ActualizarInformacion(TablaFutbol, ColumnaFutbol, LigaBetplay, ID)
        
    def EliminarDatos(self, Tabla, ID):
        """Eliminar Datos

        Argumentos:
            Me elimina los datos deseados
        """
        self.EliminarInformacion(Tabla, ID)