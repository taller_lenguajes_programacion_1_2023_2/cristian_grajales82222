from Conexion import Conexion

class Asignatura(Conexion):
    def __init__(self, nombre_asignatura="", id_profesor=""):
        Conexion.__init__(self)
        """En esta clase llamo a la clase conexion para generar una tabla de asignatura le doy el nombre y lo que voy a ingresar en la misma
        """
        self.nombre_asignatura=nombre_asignatura
        self.id_profesor=id_profesor
        self.crearTabla("Asignaturas","Asignatura")
        
    def agregarDatos(self):
        """Le doy los datos que voy a ingregar y despues lo llamo en la calse de interfaz correspondiente para su efectiva ejecucuion
        """
        datos = [
            (f"{self.nombre_asignatura}",f"{self.id_profesor}")
        ]
        self.insertarDatos("Asignaturas",2,datos)
        
    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("Asignaturas")
        