from Conexion import Conexion

class Profesor(Conexion):
    def __init__(self, nombre_profesor="", especializacion=""):
        Conexion.__init__(self)
        """En esta clase llamo a la clase conexion para generar una tabla de asignatura le doy el nombre y lo que voy a ingresar en la misma
        """
        self.nombre_profesor=nombre_profesor
        self.especializacion=especializacion
        self.crearTabla("Profesores","Profesor")
        
    def agregarDatos(self):
        """Le doy los datos que voy a ingregar y despues lo llamo en la calse de interfaz correspondiente para su efectiva ejecucuion
        """
        datos = [
            (f"{self.nombre_profesor}",f"{self.especializacion}")
        ]
        self.insertarDatos("Profesores",2,datos)
        
    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("Profesores")
        