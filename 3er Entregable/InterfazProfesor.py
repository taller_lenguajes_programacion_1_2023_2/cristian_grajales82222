import tkinter as tk
from tkinter import messagebox, ttk
from Profesor import Profesor
import sv_ttk

#Importo las clases necesarias para la ejecucion de la interfaz
class InterfazProfesor:
    def __init__(self):
        """Me genera la ventana de la interfaz profesor en la que vamos a ingresar los datos donde se colocaran los botones, los espacion y le agreg un nombre y un icono a la ventana
        """
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("Base de Datos Entregable")
        self.ventanaPrincipal.resizable(False, False)
        self.ventanaPrincipal.iconbitmap("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/3er Entregable/icono.ico")
        self.frameDatosProfesor = tk.Frame(self.ventanaPrincipal)
        self.frameDatosProfesor.grid(row=0, column=0, padx=5, pady=5)
        self.frameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        self.framebaseDatos = tk.Frame(self.ventanaPrincipal)
        self.framebaseDatos.grid(row=2, column=0)
        self.framebotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.framebotonesLateral.grid(row=3, column=0, padx=5, pady=5)
        
        """Le agrego el color oscuro y llamo las funciones para que se agreguen en la ventana principal que es la interfaz
        """
        sv_ttk.use_dark_theme()
        self.profesor=Profesor()
        self.botonesSuperiores()
        self.espaciosTexto()
        self.crearTabla()
        self.vincularbaseDatos()
        self.botonesLateral()
        self.ventanaPrincipal.mainloop()
        
    def crearTabla(self):
        """Con esta funcion se crea la ventana de la interfaz y me agrega la columnas correspondientes para la interfaz
        """
        self.tablabaseDatos = ttk.Treeview(self.framebaseDatos, show="headings")
        self.tablabaseDatos.config(columns=("ID", "Nombre Profesor", "Especializacion"))
        self.tablabaseDatos.heading("ID", text="ID")
        self.tablabaseDatos.heading("Nombre Profesor", text="NOMBRE_PROFESOR")
        self.tablabaseDatos.heading("Especializacion", text="ESPECIALIZACION")
        
        self.tablabaseDatos.grid(row=0, column=0)
        
    def espaciosTexto(self):
        """Esta funcion la utilizo para generar los espacios en los que voy a ingresar los datos con los que voy a llenar la ventana
        """
        self.variableNombre_Profesor = tk.StringVar()
        self.textoNombre_Profesor = tk.Label(self.frameDatosProfesor, text="Nombre Profesor: ")
        self.textoNombre_Profesor.grid(row=0, column=0)
        self.cuadroNombre_Profesor = tk.Entry(self.frameDatosProfesor, textvariable=self.variableNombre_Profesor)
        self.cuadroNombre_Profesor.grid(row=0, column=1)
        
        self.variableEspecializacion = tk.StringVar()
        self.textoEspecializacion = tk.Label(self.frameDatosProfesor, text="Especializacion: ")
        self.textoEspecializacion.grid(row=1, column=0)
        self.cuadroEspecializacion = tk.Entry(self.frameDatosProfesor, textvariable=self.variableEspecializacion)
        self.cuadroEspecializacion.grid(row=1, column=1)
        
    def botonesSuperiores(self):
        """Aqui genero los botones (Nuevo, Guardar y Cancelar) los cuales voy a utilizar para darle una mejor utilizacion de la interfaz
        """
        self.botonNuevo = tk.Button(self.frameBotonesSuperior, text = "Nuevo", background="green", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)
        
        self.botonGuardar = tk.Button(self.frameBotonesSuperior, text = "Guardar", background="green", command=self.funcionGuardar)
        self.botonGuardar.grid(row=0, column=1, padx=5)
        
        self.botonCancelar = tk.Button(self.frameBotonesSuperior, text = "Cancelar", background="green", command=self.funcionCancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)
      
    def botonesLateral(self):
        """Con esta funcion Creo el espacion del Id para ingresar la fila que se encuentra en este Id y genero los botones de editar y aeliminar para ejecutarlos en la ventana
        """
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.framebotonesLateral, text="ID: ")
        self.textoID.grid(row=1, column=0, padx=7)
        self.cuadroID = tk.Entry(self.framebotonesLateral, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=1, pady=5)
        
        self.botonEditar = tk.Button(self.framebotonesLateral, text = "Editar", background="green", command=self.funcionEditar)
        self.botonEditar.grid(row=1, column=2, padx=10)
        
        self.botonEliminar = tk.Button(self.framebotonesLateral, text = "Eliminar", background="green", command=self.funcionEliminar)
        self.botonEliminar.grid(row=1, column=3, padx=0)
    
    def funcionNuevo(self):
        """Esta funcion me permite dejar coomo inice las espacios y generar un nuevo ingreso
        """
        self.variableNombre_Profesor.set("")
        self.variableEspecializacion.set("")
        self.cuadroNombre_Profesor.config(state="normal")
        self.cuadroEspecializacion.config(state="normal")   
        self.botonGuardar.config(state="normal")
        self.botonCancelar.config(state="normal")  
        
    def funcionCancelar(self):
        """La funcion cancelar es para desabilitar los espacios y cancelar lo que estaba realizando
        """
        self.variableNombre_Profesor.set("")
        self.variableEspecializacion.set("")      
        self.cuadroNombre_Profesor.config(state="disabled")
        self.cuadroEspecializacion.config(state="disabled")
        self.botonGuardar.config(state="disabled")
        self.botonCancelar.config(state="disabled")
        
    def funcionGuardar(self):
        """En esta funcion guardo los datos para que me aparezcan tanto en la ventana de interfaz como en la base de datos
        """
        self.profesor.nombre_profesor = self.variableNombre_Profesor.get()
        self.profesor.especializacion = self.variableEspecializacion.get()
        self.profesor.agregarDatos()
        self.vincularbaseDatos()
        
    def funcionEditar(self):
        """Aqui puedo editar los datos ya ingresados a la tabla por un posible error a la hora de ingresarlos
        """
        if self.variableNombre_Profesor.get():
            self.profesor.actualizarDatos("Profesores", "NOMBRE_PROFESOR", self.variableNombre_Profesor.get(), self.variableID.get())
            
        if self.variableEspecializacion.get():
            self.profesor.actualizarDatos("Profesores", "ESPECIALIZACION", self.variableEspecializacion.get(), self.variableID.get())
            
        self.vincularbaseDatos()
        
    def funcionEliminar(self):
        """Esta me permite eliminar una fila segun su Id que por ejemplo ya no necesitemos en nuestra tabla
        """
        self.profesor.eliminarDatos("Profesores", self.variableID.get())
        self.vincularbaseDatos()
        
    def vincularbaseDatos(self):
        """Esta es la funcion encargada de vincular la interfaz con la base de datos y que lo que ingrese en la ventana de interfaz al guardarlo me aparezca tambien en la base de datos
        """
        self.profesor.verTabla()
        self.tablabaseDatos.delete(*self.tablabaseDatos.get_children())
        for fila in self.profesor.datosEnLaTabla:
            self.tablabaseDatos.insert("", "end", values=fila)
            
        messagebox.showinfo("AVISO", "Los datos ingresados se han vinculado correctamente con la base de datos")
        
aplicacion = InterfazProfesor()