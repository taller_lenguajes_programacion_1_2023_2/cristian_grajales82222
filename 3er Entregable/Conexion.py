import sqlite3
import json

#En esta clase se encuentra todo el codigo de las funciones para ejecutar la base de datos  el CRUD
class Conexion:
    def __init__(self):
        self.baseDatos = sqlite3.connect("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/3er Entregable/BaseDatosEntregable.sqlite")
        self.apuntador = self.baseDatos.cursor()
        with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/3er Entregable/query.json", "r") as queries:
            self.query = json.load(queries)
            
    def crearTabla(self, nombre, columnas):
        """Funcion para crear la tabla ( Base de Datos )
        """
        queryCrearTabla = self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.apuntador.execute(queryCrearTabla)
        self.baseDatos.commit()
        
    def insertarDatos(self, tabla, columnas, datos):
        """Funcion para insertar datos a la tabla ( Base de Datos )
        """
        queryinsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
        self.apuntador.executemany(queryinsertarDatos, datos)
        self.baseDatos.commit()
        
    def seleccionarTabla(self, tabla):
        """Funcion para seleccionar la tabla ( Base de Datos ) para realizar cambios
        """
        queryseleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.apuntador.execute(queryseleccionarTabla)
        contenido = self.apuntador.fetchall()
        return contenido
    
    def actualizarDatos(self, tabla, columna, nuevovalor, id):
        """Funcion que me permite actualizar los datos deseados de la tabla ( Base de Datos )
        """
        queryActualizarDatos = self.query["ActualizarDatos"].format(tabla, columna, nuevovalor, 'id', id)
        self.apuntador.execute(queryActualizarDatos)
        self.baseDatos.commit()
        
    def eliminarDatos(self, tabla, id):
        """Funcion para eliminar los datos que no necesitemos de la tabla ( Base de Datos )
        """
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla, 'ID', id)
        self.apuntador.execute(queryEliminarDatos)
        self.baseDatos.commit()
        