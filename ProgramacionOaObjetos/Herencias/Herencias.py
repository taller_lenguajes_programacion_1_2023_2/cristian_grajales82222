class animal():
    edad=0
    tamaño=0
    
    def edadTamaño(self):
        print("Seleccion satisfactoria.")
        
class perro(animal):
    raza=""
    def __init__(self, raza, edad, tamaño):
        self.edad=edad
        self.raza=raza
        self.tamaño=tamaño
        
    def edadTamaño(self):
        return super().edad
    
    def __str__(self):
        return f"La raza del perro es: {self.raza} \nLa edad del perro es: {self.edad} \nEl tamaño del perro es: {self.tamaño}"
    
perro1=perro("Doberman", 5, 17)
perro1.edadTamaño
print(perro1)

        