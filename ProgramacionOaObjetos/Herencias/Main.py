from Perro import Perro
from Gato import Gato
from Pajaro import Pajaro
from Animal import Animal
print("\n\t* SESION #10 *\n")
print("» HERENCIA «\n")
Max=Perro("\nCafe","Domestico","Terrestre","Ladrar","Doberman","Max")
Rocky=Gato("\nBlanco","Domesstico","Terrestre","Maullar","Siames","Rocky")
Aguila=Pajaro("\nBlanco-negro","Bosques","Volar","Chilla","Aguila")
print(Max.Caracteristicas())
print(Max.Morfologia())
print(Rocky.Caracteristicas())
print(Rocky.Morfologia())
print(Aguila.Caracteristicas())
print(Aguila.Morfologia())
print("\n» POLIMORFISMO «\n")
Animal.HacerComer(Max)
Animal.HacerComer(Rocky)
Animal.HacerHablar(Max)
Animal.HacerHablar(Rocky)
Animal.HacerJugar(Max)
Animal.HacerJugar(Rocky)
Animal.HacerHablar(Aguila)
Animal.HacerComer(Aguila)