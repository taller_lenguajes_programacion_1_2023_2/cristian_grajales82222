from Animal import Animal
class Pajaro(Animal):
    def __init__(self, color, habitat, movilidad, comunicacion, especie):
        self.especie=especie
        super().__init__(color, habitat, movilidad, comunicacion)
    def Caracteristicas(self):
        return f"\n| PAJARO |\nEspecie: {self.especie}"
    def Comer(self):
        print(f"A el {self.especie} le gustan las aves")
    def Hablar(self):
        print(f"El {self.especie} esta chillando")