from sqlMain import BasedeDatos

class TablaEstudios(BasedeDatos):
    def __init__(self):
        super().__init__()
        
    def CrearTablaEstudios(self):
        self.CrearTabla("Estudios", "ColumnaEstudios")
        
    def AgregarDatos(self, Carrera, Inicio, Final):
        Datos=[
            (f"{Carrera}", f"{Inicio}", f"{Final}")
        ]
        self.InsertarDatos("Estudios", "?, ?, ?", Datos)