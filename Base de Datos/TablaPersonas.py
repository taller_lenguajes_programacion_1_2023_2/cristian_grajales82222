from sqlMain import BasedeDatos

class TablaPersonas(BasedeDatos):
    def __init__(self):
        super().__init__()
        
    def CrearTablaPersonas(self):
        self.CrearTabla("Personas", "ColumnaPersonas")
        
    def AgregarDatos(self, Cedula, Nombre, Apellido):
        Datos=[
            (f"{Cedula}", f"{Nombre}", f"{Apellido}")
        ]
        self.InsertarDatos("Personas", "?, ?, ?", Datos)