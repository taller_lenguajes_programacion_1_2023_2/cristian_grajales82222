import tkinter as tk
from tkinter import messagebox, ttk
from Pelicula import Pelicula

class Interfaz:
    def __init__(self):
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("Base de Datos")
        self.ventanaPrincipal.resizable(False, False)
        self.frameDatosPelicula = tk.Frame(self.ventanaPrincipal)
        self.frameDatosPelicula.grid(row=0, column=0, padx=5, pady=5)
        self.frameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        self.framebaseDatos = tk.Frame(self.ventanaPrincipal)
        self.framebaseDatos.grid(row=2, column=0)
        self.framebotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.framebotonesLateral.grid(row=2, column=1, padx=5, pady=5)
        
        self.pelicula=Pelicula()
        self.botonesSuperiores()
        self.camposdeTexto()
        self.crearTabla()
        self.vincularbaseDatos()
        self.botonesLateral()
        self.ventanaPrincipal.mainloop()
        
    def crearTabla(self):
        self.tablabaseDatos = ttk.Treeview(self.framebaseDatos, show="headings")
        self.tablabaseDatos.config(columns=("ID", "Nombre", "Duracion", "Genero"))
        self.tablabaseDatos.heading("ID", text="ID")
        self.tablabaseDatos.heading("Nombre", text="NOMBRE")
        self.tablabaseDatos.heading("Duracion", text="DURACION")
        self.tablabaseDatos.heading("Genero", text="GENERO")
        self.tablabaseDatos.grid(row=0, column=0)
        
    def camposdeTexto(self):
        self.variableNombre = tk.StringVar()
        self.textoNombre = tk.Label(self.frameDatosPelicula, text="Nombre: ")
        self.textoNombre.grid(row=0, column=0)
        self.cuadroNombre = tk.Entry(self.frameDatosPelicula, textvariable=self.variableNombre)
        self.cuadroNombre.grid(row=0, column=1)
        
        self.variableDuracion = tk.StringVar()
        self.textoDuracion = tk.Label(self.frameDatosPelicula, text="Duracion: ")
        self.textoDuracion.grid(row=1, column=0)
        self.cuadroDuracion = tk.Entry(self.frameDatosPelicula, textvariable=self.variableDuracion)
        self.cuadroDuracion.grid(row=1, column=1)
        
        self.variableGenero = tk.StringVar()
        self.textoGenero = tk.Label(self.frameDatosPelicula, text="Genero: ")
        self.textoGenero.grid(row=2, column=0)
        self.cuadroGenero = tk.Entry(self.frameDatosPelicula, textvariable=self.variableGenero)
        self.cuadroGenero.grid(row=2, column=1)
        
    def botonesSuperiores(self):
        self.botonNuevo = tk.Button(self.frameBotonesSuperior, text = "Nuevo", background="green", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)
        
        self.botonGuardar = tk.Button(self.frameBotonesSuperior, text = "Guardar", background="green", command=self.funcionGuardar)
        self.botonGuardar.grid(row=0, column=1, padx=5)
        
        self.botonCancelar = tk.Button(self.frameBotonesSuperior, text = "Cancelar", background="green", command=self.funcionCancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)
      
    def funcionNuevo(self):
        self.variableNombre.set("")
        self.variableDuracion.set("")
        self.variableGenero.set("")
        self.cuadroNombre.config(state="normal")
        self.cuadroDuracion.config(state="normal")
        self.cuadroGenero.config(state="normal")     
        self.botonGuardar.config(state="normal")
        self.botonCancelar.config(state="normal")  
        
    def funcionCancelar(self):
        self.variableNombre.set("")
        self.variableDuracion.set("")
        self.variableGenero.set("")       
        self.cuadroNombre.config(state="disabled")
        self.cuadroDuracion.config(state="disabled")
        self.cuadroGenero.config(state="disabled")     
        self.botonGuardar.config(state="disabled")
        self.botonCancelar.config(state="disabled")
        
    def funcionGuardar(self):
        self.pelicula.nombre = self.variableNombre.get()
        self.pelicula.duracion = self.variableDuracion.get()
        self.pelicula.genero = self.variableGenero.get()
        self.pelicula.agregarDatos()
        self.vincularbaseDatos()
        
    def vincularbaseDatos(self):
        self.pelicula.verTabla()
        self.tablabaseDatos.delete(*self.tablabaseDatos.get_children())
        for fila in self.pelicula.datosEnLaTabla:
            self.tablabaseDatos.insert("", "end", values=fila)
            
    def botonesLateral(self):
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.framebotonesLateral, text="ID: ")
        self.textoID.grid(row=0, column=0)
        self.cuadroID = tk.Entry(self.framebotonesLateral, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=0)
        
        self.botonEditar = tk.Button(self.framebotonesLateral, text = "Editar", background="green", command=self.funcionEditar)
        self.botonEditar.grid(row=3, column=0, pady=5)
        
        self.botonEliminar = tk.Button(self.framebotonesLateral, text = "Eliminar", background="green", command=self.funcionEliminar)
        self.botonEliminar.grid(row=4, column=0, pady=5)
        
    def funcionEditar(self):
        if self.variableNombre.get():
            self.pelicula.actualizarDatos("Peliculas", "NOMBRE", self.variableNombre.get(), self.variableID.get())
            
        if self.variableDuracion.get():
            self.pelicula.actualizarDatos("Peliculas", "DURACION", self.variableDuracion.get(), self.variableID.get())
            
        if self.variableGenero.get():
            self.pelicula.actualizarDatos("Peliculas", "GENERO", self.variableGenero.get(), self.variableID.get())
            
        self.vincularbaseDatos()
        
    def funcionEliminar(self):
        self.pelicula.eliminarDatos("Peliculas", self.variableID.get())
        self.vincularbaseDatos()
        
aplicacion = Interfaz()
        
        