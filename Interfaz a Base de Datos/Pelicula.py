from Conexion import Conexion

class Pelicula(Conexion):
    def __init__(self, nombre="", duracion="", genero=""):
        Conexion.__init__(self)
        self.nombre=nombre
        self.duracion=duracion
        self.genero=genero
        self.crearTabla("Peliculas","Pelicula")
        
    def agregarDatos(self):
        datos = [
            (f"{self.nombre}",f"{self.duracion}",f"{self.genero}")
        ]
        self.insertarDatos("Peliculas",3,datos)
        
    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("Peliculas")
        