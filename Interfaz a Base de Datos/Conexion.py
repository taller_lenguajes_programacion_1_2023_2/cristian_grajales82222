
import sqlite3
import json

class Conexion:
    def __init__(self):
        self.baseDatos = sqlite3.connect("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Interfaz a Base de Datos/DataBase.sqlite")
        self.apuntador = self.baseDatos.cursor()
        with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/Interfaz a Base de Datos/query.json", "r") as queries:
            self.query = json.load(queries)
            
    def crearTabla(self, nombre, columnas):
        queryCrearTabla = self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.apuntador.execute(queryCrearTabla)
        self.baseDatos.commit()
        
    def insertarDatos(self, tabla, columnas, datos):
        queryinsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
        self.apuntador.executemany(queryinsertarDatos, datos)
        self.baseDatos.commit()
        
    def seleccionarTabla(self, tabla):
        queryseleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.apuntador.execute(queryseleccionarTabla)
        contenido = self.apuntador.fetchall()
        return contenido
    
    def actualizarDatos(self, tabla, columna, nuevovalor, id):
        queryActualizarDatos = self.query["ActualizarDatos"].format(tabla, columna, nuevovalor, 'id', id)
        self.apuntador.execute(queryActualizarDatos)
        self.baseDatos.commit()
        
    def eliminarDatos(self, tabla, id):
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla, 'ID', id)
        self.apuntador.execute(queryEliminarDatos)
        self.baseDatos.commit()
        
        

