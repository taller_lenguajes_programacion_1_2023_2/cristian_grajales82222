#Escribe un programa que determine si una variable dada n es par o impar

print("1.(5) Escribe un programa que determine si una variable dada n es par o impar")
ccgs_n=int(input("Ingrese su numero."))
print ("El numero es: ", ccgs_n)

if ccgs_n %2==0:
    print("Su numero es par.")
else:
    print("Su numero es impar.")
print("\n")

#Escribe un programa que determine si una cadena s es un palindromo o no
print("2.(15) Escribe un programa que determine si una cadena s es un palindromo o no")
def esPalindromo(ccgs_s):
    ccgs_s = ccgs_s.lower()
    ccgs_s = ccgs_s.replace(" ", "")
    
    a = 0
    b= len(ccgs_s) - 1
    
    for i in range(0, len(ccgs_s)):
        if ccgs_s[a] == ccgs_s[b]:
            a += 1
            b -= 1
        else:
            return False
    return True
ccgs_s = input("Ingrese una palabra para saber si es palindromo: ")
if esPalindromo(ccgs_s):
    print("La palabra", ccgs_s, "es un palindromo")
else: 
    print("La palabra", ccgs_s, "no es un palindromo")
print("\n")

#Dada una lista, escribe un programa que devuelva la suma de todos los elementos
import numpy as np
print("3.(25) Dada una lista, escribe un programa que devuelva la suma de todos los elementos")
ccgs_lista=[10, 20, 30, 40, 50]
print("\nLa suma de los elementos de la lista es: ")
print(np.sum(ccgs_lista))
print("\n")

#Escribe un programa que combine dos diccionarios en uno
import pandas as pd
print("4.(35) Escribe un programa que combine dos diccionarios en uno")
ccgs_diccionario1={
    "Nombre": "Cristian",
    "Apellido": "Grajales"
}
ccgs_diccionario2={
    "Edad": 20,
    "Documento": 1001
}
ccgs_diccionario1.update(ccgs_diccionario2)
print(ccgs_diccionario1)
print("\n")

#Escribe un programa que añada una nueva fila a un archivo CSV existente
print("5.(45) Escribe un programa que añada una nueva fila a un archivo CSV existente")
print("Creacion archivo CSV")
with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/1er_entregable/punto5(45)", "w") as csv:
    csv.write("Cristian Camilo Grajales Serna")
print("Agregacion de fila al archivo CSV existente(anterior)")
with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/1er_entregable/punto5(45)", "a") as csv:
    csv.write("\nTengo 20 años")
with open("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/1er_entregable/punto5(45)", "r") as csv:
    print(csv.read())

print("\n")

#Simula un archivo CSV, escribe un programa que agrupe los datos por una columna especifica y calcule la suma de otra columna para cada grupo
print("6.(55) Simula un archivo CSV, escribe un programa que agrupe los datos por una columna especifica y calcule la suma de otra columna para cada grupo")

print("\n")

#Simula un archivo excel Empleados.xlsx, escribe un programa que filtre y muestre solo aquellos empleados cuyo salario sea mayor a 3000
print("7. (65) Simula un archivo excel Empleados.xlsx, escribe un programa que filtre y muestre solo aquellos empleados cuyo salario sea mayor a 3000")
ccgs_datosParaExcel=pd.DataFrame({
    "Nombres":["Cristian", "Andres", "Juanjo", "Juliana"],
    "Edades":[20, 20, 19, 18],
    "Salario":[4000, 2500, 7000, 3500]
})
ccgs_datosParaExcel.to_excel("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/1er_entregable/Empleados.xlsx")
ccgs_filtroSalario=ccgs_datosParaExcel.loc[ccgs_datosParaExcel["Salario"]>3000]
print(ccgs_filtroSalario)

print("\n")

#Escribe un programa que lea un archivo excel y cambie el nombre de una columna en especifico
print("8. (75) Escribe un programa que lea un archivo excel y cambie el nombre de una columna en especifico")
ccgs_excel={
    "Nombres":["Cristian", "Andres", "Juanjo", "Juliana"],
    "Edades":[20, 20, 19, 18],
    "Salario":[4000, 2500, 7000, 3500]
}
ccgs_LecturaArchivoExcel=pd.DataFrame(ccgs_excel)
ccgs_renombrar=ccgs_LecturaArchivoExcel.rename(columns={2: "Sueldo"})
print(ccgs_renombrar)
print("\n")

#Simula un archivo CSV productos.csv escribe un programa que muestre los productos que se vendieron mas de 1000 veces pero que tienen una calificacion promedio menor a 3 
print("9. (85) Simula un archivo CSV productos.csv escribe un programa que muestre los productos que se vendieron mas de 1000 veces pero que tienen una calificacion promedio menor a 3 ")
ccgs_datosParaCSV=pd.DataFrame({
    "Productos":["Camisetas", "Guayos", "Balones", "Pantalonetas", "Espinilleras", "Medias", "Guantes"],
    "Vendidos":[1050, 900, 5000, 1700, 800, 1200, 2500], 
    "Calificacion":[2, 4, 5, 3, 2, 1, 1.5]  
})

ccgs_datosParaCSV.to_csv("C:/Users/Fredy Grajales/Desktop/Ing. Informatica/3° SEMESTRE/Algoritmos y Progrmacion 3/Taller de Lenguajes 1/cristian_grajales82222/1er_entregable/productos.csv")
ccgs_filtros=ccgs_datosParaCSV.loc[ccgs_datosParaCSV["Vendidos"]>1000][ccgs_datosParaCSV["Calificacion"]<3]
print(ccgs_filtros)
print("\n")

#Escribe un programa que lea un archivo CSV y utilice la funcion merge para combinarlo con otro CSV bsado en una columna en comun 
print("10. (95) Escribe un programa que lea un archivo CSV y utilice la funcion merge para combinarlo con otro CSV bsado en una columna en comun")
ccgs_archivoCSV=pd.DataFrame({
    "Ciudades":["Medellin", "Bogota", "Rionegro"],
    "Equipos":["Nacional", "Santa Fe", "Aguilas"]
})
ccgs_archivoCSV1=pd.DataFrame({
    "Ciudades":["Medellin", "Bogota", "Rionegro"],
    "Goles":[6000, 4000, 2500]
})
print(pd.merge(ccgs_archivoCSV, ccgs_archivoCSV1, on="Ciudades"))